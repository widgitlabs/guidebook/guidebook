module.exports = {
    // https://eslint.org/docs/user-guide/configuring#configuration-cascading-and-hierarchy
    // This option interrupts the configuration hierarchy at this file
    // Remove this if you have an higher level ESLint config file (it usually happens into a monorepos)
    root: true,
    parserOptions: {
        ecmaVersion: "latest", // Allows for the parsing of modern ECMAScript features
        sourceType: "module",
    },
    env: {
        node: true,
        browser: true,
        es2020: true,
    },
    extends: [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:react/jsx-runtime",
        "plugin:react-hooks/recommended",
        "prettier",
    ],
    settings: {
        react: {
            version: "18.2",
        },
    },
    plugins: ["react-refresh"],
    globals: {
        process: "readonly",
        chrome: "readonly",
    },
    rules: {
        "prefer-promise-reject-errors": "off",
        "react-refresh/only-export-components": "warn",

        // allow debugger during development only
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    },
};
