# Guidebook

[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/widgitlabs/guidebook/guidebook/blob/main/LICENSE.txt)
[![Pipelines](https://gitlab.com/widgitlabs/guidebook/guidebook/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/guidebook/guidebook/pipelines)
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)](https://discord.gg/jrydFBP)
