export function Icon() {
    return (
        <svg
            id="guidebook-icon"
            width="256mm"
            height="256mm"
            version="1.1"
            viewBox="0 0 256 256"
            xmlns="http://www.w3.org/2000/svg"
        >
            <rect
                id="background"
                y="-7.1054e-15"
                width="256"
                height="256"
                fill="#fafaff"
                strokeWidth="0"
            />
            <path
                id="border"
                d="m0 0v256h256v-256zm22.5 22.5h211v211h-211z"
                fill="#247ba0"
                strokeWidth="0"
            />
            <text
                id="g"
                x="37.793877"
                y="208.67403"
                fill="#1a1a1a"
                fontFamily="Outfit"
                fontSize="232.49px"
                letterSpacing="1.5671px"
                strokeWidth=".28494"
                xmlSpace="preserve"
            >
                <tspan
                    id="tspan1"
                    x="37.793877"
                    y="208.67403"
                    fill="#1a1a1a"
                    fontFamily="Outfit"
                    fontSize="232.49px"
                    strokeWidth=".28494"
                >
                    G
                </tspan>
            </text>
        </svg>
    );
}

export function Logo() {
    return (
        <svg
            id="guidebook-logo"
            width="1278mm"
            height="256mm"
            version="1.1"
            viewBox="0 0 1278 256"
            xmlns="http://www.w3.org/2000/svg"
        >
            <title>GuideBook</title>
            <rect id="backgroundr" width="1278" height="256" fill="#fafaff" />
            <rect
                id="backgroundl"
                y="-7.1054e-15"
                width="256"
                height="256"
                fill="#fafaff"
                strokeWidth="0"
            />
            <path
                id="border"
                d="m0 0v256h256v-256zm22.5 22.5h211v211h-211z"
                fill="#247ba0"
                strokeWidth="0"
            />
            <text
                id="text"
                transform="scale(.99962 1.0004)"
                x="37.779354"
                y="208.59384"
                fill="#1a1a1a"
                fontFamily="Outfit"
                fontSize="232.4px"
                letterSpacing="1.5671px"
                strokeWidth=".28483"
                xmlSpace="preserve"
            >
                <tspan
                    id="tspan1"
                    x="37.779354"
                    y="208.59384"
                    fill="#1a1a1a"
                    fontFamily="Outfit"
                    fontSize="232.4px"
                    strokeWidth=".28483"
                >
                    G uideBook
                </tspan>
            </text>
        </svg>
    );
}
