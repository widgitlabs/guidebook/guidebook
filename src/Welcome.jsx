import { Icon } from "./Branding.jsx";
import Input from "./Input";
import Button from "./Button";

export default function Welcome() {
    return (
        <div className="welcome">
            <div className="details">
                <Icon />
                <h1>Welcome to GuideBook</h1>
                <p>A simple, self-contained platform for managing guides</p>
            </div>
            <div className="setup">
                <form>
                    <Input
                        label="Site Title"
                        placeholder="My Awesome Guide"
                        type="text"
                        required
                    />
                    <Input
                        label="Username"
                        placeholder="jsmith"
                        type="text"
                        required
                    />
                    <Input
                        label="Email"
                        placeholder="jsmith@example.com"
                        type="email"
                        required
                    />
                    <Input label="Password" type="password" required />
                    <Button type="submit">Install</Button>
                </form>
            </div>
        </div>
    );
}
