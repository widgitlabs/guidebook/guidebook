import clsx from "clsx";
import PropTypes from "prop-types";

export default function Input(props) {
    const {
        className,
        label,
        placeholder,
        required,
        type = "text",
        ...rest
    } = props;

    const classNames = clsx({ input: true }, className);

    return (
        <label className="label">
            {label}
            {required && <span className="input-required">*</span>}
            <div>
                <input
                    type={type}
                    placeholder={placeholder}
                    className={classNames}
                    required={required}
                    {...rest}
                />
            </div>
        </label>
    );
}

Input.propTypes = {
    className: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    required: PropTypes.string,
    type: PropTypes.string,
};
