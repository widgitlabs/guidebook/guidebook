import { BrowserRouter, Routes, Route } from "react-router-dom";
import Welcome from "./Welcome";

function App() {
    return (
        <BrowserRouter>
            <div id="guidebook">
                <Routes>
                    <Route path="/" element={<Welcome />}></Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
