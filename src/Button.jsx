import clsx from "clsx";
import PropTypes from "prop-types";

export default function Button(props) {
    const { children, outline, className, ...rest } = props;

    const classNames = clsx(
        {
            btn: true,
            "btn-default": !outline,
            "btn-outline": outline,
        },
        className
    );

    return (
        <button className={classNames} {...rest}>
            {children}
        </button>
    );
}

Button.propTypes = {
    children: PropTypes.node,
    outline: PropTypes.bool,
    className: PropTypes.string,
};
