import { NavLink } from "react-router-dom";
import { Logo } from "./Branding.jsx";

export default function Navbar() {
    return (
        <nav className="navbar">
            <NavLink to="/" className="nav-brand">
                <Logo />
            </NavLink>
            <ul>
                <li className="nav-item">
                    <NavLink
                        end
                        className={({ isActive }) => (isActive ? "active" : "")}
                        to="/"
                    >
                        Home
                    </NavLink>
                </li>
                <li className="nav-item">
                    <NavLink
                        end
                        className={({ isActive }) => (isActive ? "active" : "")}
                        to="/about"
                    >
                        About us
                    </NavLink>
                </li>
            </ul>
        </nav>
    );
}
