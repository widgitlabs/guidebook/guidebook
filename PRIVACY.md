# Privacy

This is a plain English summary of all of the components within Guidebook which
may affect your privacy in some way. Please keep in mind that if you use third
party addons with Guidebook, there may be additional things not listed here.

## Official Services

Some official services are enabled by default. These services connect to our
distribution server and are managed by Widgit Labs.

### Automatic Update Checks

When a new session is started, Guidebook pings a Widgit Labs service to check
if currently installed version of Guidebook is the latest version. If an
updated version is available, a notification on appears in the Guidebook
dashboard to let you know.

We collect basic anonymised usage statistics from your site before sending the
request to the service. You can disable collecting statistics using the
privacy configuration. You will still receive notifications from the service.

## Third Party Services

Guidebook uses a number of third party services for specific funtionality.

### Gravatar

To automatically populate your profile picture, Guidebook pings [Gravatar] to
see if your email address is associated with a profile. If it is, we pull in
your profile picture. If not, a default image is used.

### Structured Data

Guidebook outputs basic meta tags to allow rich snippets of your content to be
recognised by popular social networks.

-   [Schema.org]
-   [Open Graph]
-   [Twitter cards]

[Gravatar]: http://gravatar.com/
[Schema.org]: http://schema.org/docs/documents.html
[Open Graph]: http://ogp.me/
[Twitter cards]: https://dev.twitter.com/cards/overview
